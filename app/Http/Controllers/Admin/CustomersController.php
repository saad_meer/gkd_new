<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Admin\Address;
use Validator;
use DB;
class CustomersController extends Controller
{
    public function __construct(Address $address)
    {
        $this->address = $address;
    }
    public function profile()
    {
        return view('users.changepassword');
    }
    public function profileProcess(Request $request)
    {
        $user=User::where('id','=',auth()->user()->id)->first();
        if($user)
        {
            $validator = Validator::make($request->all(), [
                'current_password' => 'required',
                'new_password' =>   'required',
                'new_confirm_password' =>   'required:new_password',
            ],
            [
                'current_password.required' => 'Current Password is Required',
                'new_password.required' => 'New Password is Required',
                'new_confirm_password.required' => 'Confirm Password is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            if($user->password!=bcrypt($request->current_password))
            {
                User::find(auth()->user()->id)->update(['password'=> bcrypt($request->new_password)]);
                return response()->json(['code'=>200,'message'=>'Password Changed Successfully']);
            }
            else
            {
                return response()->json(['code'=>404,'message'=>'Old Password Not Matched']);

            }


        }


    }
    public function index()
    {
        return view('customers.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'first_name',
            1 =>'email',
            2=> 'phone_number_one',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = User::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $users = User::offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->where('role_id',3)
                        ->where('user_type','customer')
                        ->get();
            }
            else {
            $search = $request->input('search.value');

            $users =  User::where('first_name','LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->orWhere('phone_number_one', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = User::where('first_name','LIKE',"%{$search}%")
                                ->orWhere('last_name', 'LIKE',"%{$search}%")
                                ->orWhere('phone_number_one', 'LIKE',"%{$search}%")
                                ->orWhere('email', 'LIKE',"%{$search}%")
                                 ->count();
            }

            $data = array();
            if(!empty($users))
            {

            foreach ($users as $value)
            {
            $edit =  url('admin/customers/edit',$value->id);

            $nestedData['name'] = $value->first_name.' '.$value->last_name;
            $nestedData['email'] =$value->email;
            $nestedData['phone_one'] = $value->phone_number_one;
            $nestedData['created_at'] =  format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('customers.add');
    }
    public function addProcess(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' =>   'required',
            'email' =>   'required|email|unique:users,email',
            'password' =>   'required',
            'phone_number_one' =>   'required',
        ],
        [
            'first_name.required' => 'First Name is Required',
            'last_name.required' => 'Last Name is Required',
            'email.required' => 'Email is Required',
            'password.required' => 'Password is Required|min:6',
            'phone_number_one.required' => 'Phone 1 is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            DB::beginTransaction();
            $user= new User();
            $user->role_id=3;
            $user->user_type='customer';
            $user->first_name=$request->first_name;
            $user->last_name=$request->last_name;
            $user->email= $request->email;
            $user->phone_number_one = $request->phone_number_one;
            $user->phone_number_two = $request->phone_number_two;
            $user->user_admin_notes = $request->user_admin_notes;
            $user->password=bcrypt($request->password);
            if ($request->hasFile('user_profile_picture')) {
                $fileName = time().'_'.$request->user_profile_picture->getClientOriginalName();
                $filePath = $request->file('user_profile_picture')->storeAs('uploads/customer', $fileName, 'public');
                $user->user_profile_picture = $filePath;
            }
            $user->save();

            if(!empty($request['addAddress']))
            {
                $this->address->addAddress($request['addAddress'],$user->id);
            }
            DB::commit();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $user=User::where('id','=',$id)->where('role_id',3)
        ->where('user_type','customer')->first();
        if($user)
        {
            $address=Address::where('user_id',$id)->get();
            return view('customers.edit',compact('user','address'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $user=User::where('id','=',$request->id)->first();
        if($user)
        {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' =>   'required',
            ],
            [
                'first_name.required' => 'First Name is Required',
                'last_name.required' => 'Last Name is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                DB::beginTransaction();
                $user->role_id=3;
                $user->user_type='customer';
                $user->first_name=$request->first_name;
                $user->last_name=$request->last_name;
                $user->phone_number_one = $request->phone_number_one;
                $user->phone_number_two = $request->phone_number_two;
                $user->user_admin_notes = $request->user_admin_notes;
                $user->password=bcrypt($request->password);
                if ($request->hasFile('user_profile_picture')) {
                    $fileName = time().'_'.$request->user_profile_picture->getClientOriginalName();
                    $filePath = $request->file('user_profile_picture')->storeAs('uploads/customer', $fileName, 'public');
                    $user->user_profile_picture = $filePath;
                }
                $user->save();
                if(!empty($request['addAddress']))
                {
                    $this->address->updateAddress($request['addAddress'],$user->id);
                }
                DB::commit();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('admin/customers/display');

    }
    public function delete($id)
    {
        $delete=User::where('id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
