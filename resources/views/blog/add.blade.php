
@extends('layouts.header')
@section('title','Add Blog')
@section('datatables')
<link href="{{url('lib/select2/css/select2.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <div class="content-body" style="overflow: hidden;">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Blog</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('blog/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Blog" class="df-example demo-forms">
                        <form id="addpage" action="{{url('blog/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Title<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="blog_title" placeholder="Title">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Category <span class="text-danger">*</span></label>
                                        <select class="custom-select select2 blog_category_id" name="blog_category_id">
                                            <option value="">Please Select Category</option>
                                            @foreach ($category as $value)
                                                <option value="{{$value->category_id}}">{{$value->category_name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>SubCategory</label>
                                        <select class="custom-select select2" name="blog_subcategory_id" id="subcategory">
                                            <option value="">Please Select Sub Category</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Author</label>
                                        <select class="custom-select select2" name="blog_author">
                                            <option value="">Please Select Author</option>
                                            @foreach ($author as $value)
                                            <option value="{{$value->id}}">{{$value->author_first_name .' '. $value->author_last_name}}</option>
                                        @endforeach
                                        </select>
                                </div>

                                    <div class="form-group col-md-6 " id="bg_image">
                                        <label>Image</label>
                                        <input type="file" class="form-control" name="blog_images" id="customFile">
                                    </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="blog_status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Discription</label>
                                    <textarea id="summernote" name="blog_text"></textarea>

                                </div>
                                <div class="col-md-12 mt-5 pt-5">
                                    <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/select2/js/select2.min.js')}}"></script>


<script type="text/javascript">

$('.select2').select2();

$(".blog_category_id").change(function() {
    var html='';
    var url = '{{url("/")}}';
    var category = $('.blog_category_id').val();
    $.ajax({
    url: url+'/getsinglesubcategory',
    type: 'POST',
    data: {
        Category: category
    },
    success: function (data) {
        if(data.code==200)
        {
            html='<option value="">Please Select Sub Category</option>';
            $.each( data.result, function( key, value ) {
                html+='<option value="'+value.id+'">'+value.subcategory_name+'</option>';
            });
            console.log(html);
            $('#subcategory').html(html);
        }
        if(data.code==404)
        {
            html='<option value="">Please Select Sub Category</option>';
            $('#subcategory').html(html);
        }
    },
    error: function (error) {
       // swalerror("SomeThing Went Wrong");
    }
});
});
$('#addpage').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#addpage');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addpage')[0].reset();
            swalsuccess(data.message,url+'/blog/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



