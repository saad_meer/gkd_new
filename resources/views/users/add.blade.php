
@extends('layouts.header')
@section('title','Add Users')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Users</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/users/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form id="addform" action="{{url('admin/users/addProcess')}}" method="POST">
                        <div data-label="Add Users" class="df-example demo-forms">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">User Type<span class="text-danger">*</span></label>
                                    <select name="user_type" id="user_type" class="form-control">
                                        <option value="admin">Admin</option>
                                        <option value="driver">Driver</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Profile Picture</label>
                                    <input type="file" class="form-control" name="user_profile_picture" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Nic Front</label>
                                    <input type="file" class="form-control" name="user_nic_front" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Nic Back</label>
                                    <input type="file" class="form-control" name="user_nic_back" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Bank Check</label>
                                    <input type="text" class="form-control" name="user_bank_check_number" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Salary</label>
                                    <input type="text" class="form-control" name="user_salary" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Petrol Per Km</label>
                                    <input type="text"  class="form-control" name="user_pertrol_per_km" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Working Hour</label>
                                    <input type="number"  class="form-control" name="user_working_hour" >
                                </div>
                            </div>
                        </div><!-- df-example -->
                        <div data-label="Add Address" class="df-example demo-forms mt-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mg-b-0 tx-spacing--1">Add Address</h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="#modal-addaddress" type="button" data-toggle="modal"  class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Add</a>
                                </div>
                                <div class="col-sm-12 mt-4">
                                    <table class="table table-striped">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>Name</th>
                                                <th>Number</th>
                                                <th>Address</th>
                                                <th>Area</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tbody id="addaddress">

                                            </tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 float-right mt-4">
                                <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div><!-- container -->
        @include('modal.ModalAddress');
    </div>


@endsection
@section('scripts')

<script type="text/javascript">


$('#addform').submit(function(event) {
    var url = '{{url("/")}}';

    //prevent the form from submitting by default
    event.preventDefault();

    var frm = $('#addform');

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: frm.attr('action'),
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            if(data.code==200)
            {
                $('#addform')[0].reset();
                swalsuccess(data.message,url+'/admin/users/display');
            }
            if(data.code==404)
            {
                swalerror(data.message);
            }
        },
        error: function (error) {
            swalerror("SomeThing Went Wrong");
        }
    });
});

</script>
@include('modal.ModalScript')
@endsection



