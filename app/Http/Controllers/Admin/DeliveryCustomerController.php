<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\DeliveryCustomer;
use Validator;

class DeliveryCustomerController extends Controller
{

    public function index()
    {
        return view('delivery_customer.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'delivery_type',
            1 =>'delivery_payment_type',
            2=> 'delivery_payment_amount',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = DeliveryCustomer::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $delivery_customer = DeliveryCustomer::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $delivery_customer =  DeliveryCustomer::where('delivery_type','LIKE',"%{$search}%")
                        ->orWhere('delivery_payment_type', 'LIKE',"%{$search}%")
                        ->orWhere('delivery_payment_amount', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = DeliveryCustomer::where('delivery_type','LIKE',"%{$search}%")
                        ->orWhere('delivery_payment_type', 'LIKE',"%{$search}%")
                        ->orWhere('delivery_payment_amount', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($delivery_customer))
            {
            foreach ($delivery_customer as $values)
            {
            $edit =  url('admin/delivery/edit',$values->id);

            $nestedData['delivery_type'] = $values->delivery_type;
            $nestedData['delivery_payment_type'] = $values->delivery_payment_type;
            $nestedData['delivery_payment_amount'] = $values->delivery_payment_amount;
            $nestedData['created_at'] = format_date_time($values->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$values->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('delivery_customer.add');
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'delivery_type' => 'required',
            'delivery_payment_amount' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $delivery_customer= new DeliveryCustomer();
            $delivery_customer->delivery_type=$request->delivery_type;
            $delivery_customer->delivery_payment_type=$request->delivery_payment_type;
            $delivery_customer->delivery_payment_amount=$request->delivery_payment_amount;
            $delivery_customer->delivery_payment_value=$request->delivery_payment_value;
            $delivery_customer->delivery_payment_schedules=$request->delivery_payment_schedules;
            $delivery_customer->delivery_pick_up_address_name=$request->delivery_pick_up_address_name;
            $delivery_customer->delivery_pick_up_address_number=$request->delivery_pick_up_address_number;
            $delivery_customer->delivery_pick_up_address_address=$request->delivery_pick_up_address_address;
            $delivery_customer->delivery_pick_up_address_city=$request->delivery_pick_up_address_city;
            $delivery_customer->delivery_pick_up_address_state=$request->delivery_pick_up_address_state;
            $delivery_customer->delivery_drop_off_address_name=$request->delivery_drop_off_address_name;
            $delivery_customer->delivery_drop_off_address_number=$request->delivery_drop_off_address_number;
            $delivery_customer->delivery_drop_off_address_address=$request->delivery_drop_off_address_address;
            $delivery_customer->delivery_drop_off_address_city=$request->delivery_drop_off_address_city;
            $delivery_customer->delivery_drop_off_address_state=$request->delivery_drop_off_address_state;
            $delivery_customer->delivery_charges=$request->delivery_charges;
            $delivery_customer->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }

    }
    public function edit($id)
    {
        $delivery_customer=DeliveryCustomer::where('id','=',$id)->first();
        if($delivery_customer)
        {
            return view('delivery_customer.edit',compact('delivery_customer'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {

        $delivery_customer=DeliveryCustomer::where('id','=',$request->id)->first();
        if($delivery_customer)
        {
            $validator = Validator::make($request->all(), [
                'delivery_type' => 'required',
                'delivery_payment_amount' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {

                $delivery_customer->delivery_type=$request->delivery_type;
                $delivery_customer->delivery_payment_type=$request->delivery_payment_type;
                $delivery_customer->delivery_payment_amount=$request->delivery_payment_amount;
                $delivery_customer->delivery_payment_value=$request->delivery_payment_value;
                $delivery_customer->delivery_payment_schedules=$request->delivery_payment_schedules;
                $delivery_customer->delivery_pick_up_address_name=$request->delivery_pick_up_address_name;
                $delivery_customer->delivery_pick_up_address_number=$request->delivery_pick_up_address_number;
                $delivery_customer->delivery_pick_up_address_address=$request->delivery_pick_up_address_address;
                $delivery_customer->delivery_pick_up_address_city=$request->delivery_pick_up_address_city;
                $delivery_customer->delivery_pick_up_address_state=$request->delivery_pick_up_address_state;
                $delivery_customer->delivery_drop_off_address_name=$request->delivery_drop_off_address_name;
                $delivery_customer->delivery_drop_off_address_number=$request->delivery_drop_off_address_number;
                $delivery_customer->delivery_drop_off_address_address=$request->delivery_drop_off_address_address;
                $delivery_customer->delivery_drop_off_address_city=$request->delivery_drop_off_address_city;
                $delivery_customer->delivery_drop_off_address_state=$request->delivery_drop_off_address_state;
                $delivery_customer->delivery_charges=$request->delivery_charges;
                $delivery_customer->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('delivery/display');

    }
    public function delete($id)
    {
        $delivery_customer=DeliveryCustomer::where('id','=',$id)->first();
        if($delivery_customer)
        {
            $delivery_customer->delete();
            return response()->json(['code'=>404,'message'=>'Delivery Customer Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
    //
}
