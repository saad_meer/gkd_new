<aside class="aside aside-fixed">
      <div class="aside-header">
        <a href="../../index.html" class="aside-logo">dash<span>forge</span></a>
        <a href="" class="aside-menu-link">
          <i data-feather="menu"></i>
          <i data-feather="x"></i>
        </a>
      </div>
      <div class="aside-body">
        <div class="aside-loggedin">
          <div class="d-flex align-items-center justify-content-start">
            <a href="" class="avatar">
                @if(auth()->user()->user_profile_picture!='')
                <img src="{{url('storage/'.auth()->user()->user_profile_picture)}}"  class="rounded-circle" alt="">
                @else
                <img src="https://via.placeholder.com/500" class="rounded-circle" alt="">

                @endif
            </a>
            <div class="aside-alert-link">
              <a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" data-toggle="tooltip" title="Sign out"><i data-feather="log-out"></i></a>
            </div>
          </div>

          <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
          <div class="aside-loggedin-user">
            <a href="#loggedinMenu" class="d-flex align-items-center justify-content-between mg-b-2" data-toggle="collapse">
              <h6 class="tx-semibold mg-b-0"></h6>
              <i data-feather="chevron-down"></i>
            </a>
            <p class="tx-color-03 tx-12 mg-b-0">{{auth()->user()->first_name.' '.auth()->user()->last_name}}</p>
          </div>
          <div class="collapse" id="loggedinMenu">
            <ul class="nav nav-aside mg-b-0">
              <li class="nav-item"><a href="{{url('users/editprofile')}}" class="nav-link"><i data-feather="edit"></i> <span>Edit Profile</span></a></li>
              <li class="nav-item"><a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link"><i data-feather="log-out"></i> <span>Sign Out</span></a></li>
            </ul>
          </div>
        </div><!-- aside-loggedin -->
        <ul class="nav nav-aside">
          <li class="nav-label">Dashboard</li>
          <li class="nav-item "><a href="{{url('admin/dashboard')}}" class="nav-link"><i data-feather="shopping-bag"></i> <span>Dashboard</span></a></li>
          <li class="nav-item {{ Request::is('admin/users/display') ? 'active' : '' }}"><a href="{{url('admin/users/display')}}" class="nav-link"><i data-feather="users"></i> <span>Users</span></a></li>

          <li class="nav-label mg-t-25">Admin Links</li>
          <li class="nav-item {{ Request::is('admin/customers/display') ? 'active' : '' }}"><a href="{{url('admin/customers/display')}}" class="nav-link"><i data-feather="users"></i> <span>Customer</span></a></li>
          <li class="nav-item {{ Request::is('admin/customers_shop/display') ? 'active' : '' }}"><a href="{{url('admin/customers_shop/display')}}" class="nav-link"><i data-feather="users"></i> <span>Customer Shop</span></a></li>
          <li class="nav-item {{ Request::is('admin/category/display') ? 'active' : '' }}"><a href="{{url('admin/category/display')}}" class="nav-link"><i data-feather="list"></i> <span>Category</span></a></li>
          <li class="nav-item {{ Request::is('admin/homeslider/display') ? 'active' : '' }}"><a href="{{url('admin/homeslider/display')}}" class="nav-link"><i data-feather="list"></i> <span>Home Slider</span></a></li>
          <li class="nav-item {{ Request::is('admin/delivery_charges/display') ? 'active' : '' }}"><a href="{{url('admin/delivery_charges/display')}}" class="nav-link"><i data-feather="list"></i> <span>Delivery Charges</span></a></li>
          <li class="nav-item {{ Request::is('admin/delivery/display') ? 'active' : '' }}"><a href="{{url('admin/delivery/display')}}" class="nav-link"><i data-feather="list"></i> <span>Delivery's</span></a></li>
          <li class="nav-item {{ Request::is('admin/expense_module/display') ? 'active' : '' }}"><a href="{{url('admin/expense_module/display')}}" class="nav-link"><i data-feather="list"></i> <span>Expence Module</span></a></li>
          <li class="nav-item {{ Request::is('admin/pages/display') ? 'active' : '' }} {{ Request::is('admin/pages/add') ? 'active' : '' }}"><a href="{{url('admin/pages/display')}}" class="nav-link"><i data-feather="file"></i> <span>Pages</span></a></li>
{{--
          <li class="nav-item {{ Request::is('sub-category/display') ? 'active' : '' }}"><a href="{{url('sub-category/display')}}" class="nav-link"><i data-feather="list"></i> <span>SubCategory</span></a></li>
          <li class="nav-item {{ Request::is('author/display') ? 'active' : '' }}"><a href="{{url('author/display')}}" class="nav-link"><i data-feather="users"></i> <span>Author</span></a></li>
          <li class="nav-item {{ Request::is('quote_template/display') ? 'active' : '' }} {{ Request::is('quote_template/add') ? 'active' : '' }}"><a href="{{url('quote_template/display')}}" class="nav-link"><i data-feather="arrow-right"></i> <span>Quotes Template</span></a></li>
          <li class="nav-item {{ Request::is('quotes/display') ? 'active' : '' }} {{ Request::is('quotes/add') ? 'active' : '' }}"><a href="{{url('quotes/display')}}" class="nav-link"><i data-feather="arrow-right"></i> <span>Quotes</span></a></li>
          <li class="nav-item {{ Request::is('blog/display') ? 'active' : '' }} {{ Request::is('blog/add') ? 'active' : '' }}"><a href="{{url('blog/display')}}" class="nav-link"><i data-feather="file"></i> <span>Blog</span></a></li> --}}
         </ul>
      </div>
    </aside>
