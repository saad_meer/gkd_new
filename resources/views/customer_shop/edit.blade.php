
@extends('layouts.header')
@section('title','Edit Customer Shop')
@section('datatables')
<link href="{{url('lib/select2/css/select2.min.css')}}" rel="stylesheet">
<link href="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

<style>
    .bootstrap-tagsinput {
        display:block !important;
    }
</style>
@endsection
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Customer Shop</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/customers_shop/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Customer Shop" class="df-example demo-forms">
                        <form id="customers_shop" action="{{url('admin/customers_shop/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Customer <span class="text-danger">*</span></label>
                                        <select class="custom-select select2" name="user_id">
                                            <option value="">Please Select Customer</option>
                                            @foreach ($user as $user)
                                                <option value="{{$user->id}}"  @if($user->id==$customer_shop->user_id) selected @endif>{{$user->first_name.''.$user->last_name}}</option>
                                            @endforeach
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Bussniss Type</label>
                                    <input type="text" class="form-control" value="{{$customer_shop->customer_business_type}}" id="customer_business_type" name="customer_business_type" data-role="tagsinput">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Bussniss Address</label>
                                    <input type="text" class="form-control" value="{{$customer_shop->customer_business_address}}" name="customer_business_address">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Bussniss Latitude</label>
                                    <input type="text" class="form-control" value="{{$customer_shop->customer_business_lat}}" name="customer_business_lat">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Bussniss Logitude</label>
                                    <input type="text" class="form-control" value="{{$customer_shop->customer_business_log}}" name="customer_business_log">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Bussniss Person Name</label>
                                    <input type="text" class="form-control"  value="{{$customer_shop->customer_business_person_name}}" name="customer_business_person_name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Bussniss Person Number</label>
                                    <input type="number" class="form-control" value="{{$customer_shop->customer_business_person_number}}" name="customer_business_person_number">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Rate Delivery in Boundary</label>
                                    <input type="number" min="0" step="0.01" class="form-control" value="{{$customer_shop->customer_rate_per_delivery_in_boundary}}" name="customer_rate_per_delivery_in_boundary">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Rate Delivery in Boundary</label>
                                    <input type="number" min="0" step="0.01" class="form-control" value="{{$customer_shop->customer_rate_per_delivery_out_boundary}}" name="customer_rate_per_delivery_out_boundary">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Customer Rate Average Number</label>
                                    <input type="number" min="0" step="0.01" class="form-control" value="{{$customer_shop->customer_rate_per_average_number_delivery_daily_base}}" name="customer_rate_per_average_number_delivery_daily_base">
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/select2/js/select2.min.js')}}"></script>
<script src="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
     $(function(){
        'use strict'

        $('#customer_business_type').tagsinput();


    });

$('.select2').select2();

$('#customers_shop').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#customers_shop');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $customer_shop->customer_shop_id; ?>');
$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#customers_shop')[0].reset();
            swalsuccess(data.message,url+'/admin/customers_shop/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



