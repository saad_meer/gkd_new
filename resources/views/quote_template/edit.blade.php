
@extends('layouts.header')
@section('title','Edit Quote Template')
@section('content')


    <div class="content-body" style="overflow: hidden;">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Quote Template</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('quote_template/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="quoto" class="df-example demo-forms">
                        <form id="editquoto" action="{{url('quote_template/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Template Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="template_name" value="{{$quote->template_name}}" placeholder="Template Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Text Font Family</label>
                                    <select class="custom-select" name="template_font_family">
                                        @foreach ($font_style as $value)
                                            <option value="{{$value->id}}" @if($value->id==$quote->template_font_family) selected @endif> {{$value->font_style}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Text Style</label>
                                    <select class="custom-select" name="template_text_style">
                                        <option value="italic" @if($quote->template_text_style=='italic') selected @endif>Italic</option>
                                        <option value="normal"  @if($quote->template_text_style=='normal') selected @endif>Normal</option>
                                        <option value="oblique"  @if($quote->template_text_style=='oblique') selected @endif>Oblique</option>
                                        <option value="initial"  @if($quote->template_text_style=='initial') selected @endif>Initial</option>
                                        <option value="Inherit"  @if($quote->template_text_style=='Inherit') selected @endif>Inherit</option>
                                </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Text Size</label>
                                    <input type="text" class="form-control" name="template_text_size" value="{{$quote->template_text_size}}" placeholder="12px">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Text Color</label>
                                    <input type="color" class="form-control" value="{{$quote->template_text_color}}" name="template_text_color">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="template_status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" @if($quote->template_has_image==1) checked @endif name="template_has_image" id="customCheck1" value="1">
                                        <label class="custom-control-label" for="customCheck1">Do You Want to Set Image as Backgroud</label>
                                      </div>
                                </div>
                                <div class="form-group col-md-6 @if($quote->template_has_image==1) d-none @endif" id="bg_color">
                                    <label>Background Color</label>
                                    <input type="color" class="form-control" name="template_background_color">
                                </div>
                                <input type="hidden" name="old_image" value="{{$quote->template_image}}">

                                <div class="form-group col-md-6 @if($quote->template_has_image==0)  d-none @endif" id="bg_image">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="template_image" id="customFile">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
                <div class="col-sm-12 mt-4">
                    @if(!empty($quote->template_image))
                    <div data-label="Preview" class="df-example demo-forms">
                        <div class="form-row">
                            <img src="{{url('storage/'.$quote->template_image)}}" class="rounded float-left w-25" alt="">
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">

$("#customCheck1").change(function() {
    if(this.checked) {
        $('#bg_color').addClass('d-none');
        $('#bg_image').removeClass('d-none');
    }
    else
    {
        $('#bg_color').removeClass('d-none');
        $('#bg_image').addClass('d-none');
    }
});
$('#editquoto').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#editquoto');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $quote->id; ?>');

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            swalsuccess(data.message,url+'/quote_template/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



