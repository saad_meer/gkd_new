<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryCharges extends Model
{
    use SoftDeletes;
    public $table = 'delivery_charges';
}
