<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserToAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_to_address', function (Blueprint $table) {
            $table->id();
            $table->Integer('user_id')->nullable();
            $table->text('address_name')->nullable();
            $table->text('address_number')->nullable();
            $table->string('address_primary',1000)->nullable();
            $table->text('address_area')->nullable();
            $table->text('address_city')->nullable();
            $table->text('address_state')->nullable();
            $table->text('address_lat')->nullable();
            $table->text('address_log')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_to_address');
    }
}
