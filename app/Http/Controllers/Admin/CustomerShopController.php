<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Admin\CustomerShop;
use Validator;

class CustomerShopController extends Controller
{
    public function index()
    {
        return view('customer_shop.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'user_id',
            1 =>'customer_business_person_name',
            2=> 'customer_business_person_number',
            3=> 'customer_rate_per_average_number_delivery_daily_base',
            4=> 'created_at',
            5=> 'customer_shop_id',
        );

            $totalData = CustomerShop::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $customer_shop = CustomerShop::Leftjoin('users','customer_shop.user_id','users.id')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $customer_shop =  CustomerShop::Leftjoin('users','customer_shop.user_id','users.id')
                        ->where('customer_business_person_name','LIKE',"%{$search}%")
                        ->orWhere('users.first_name', 'LIKE',"%{$search}%")
                        ->orWhere('users.last_name', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = CustomerShop::Leftjoin('users','customer_shop.user_id','users.id')
                        ->where('customer_business_person_name','LIKE',"%{$search}%")
                        ->orWhere('users.first_name', 'LIKE',"%{$search}%")
                        ->orWhere('users.last_name', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($customer_shop))
            {
            foreach ($customer_shop as $shop)
            {
            $edit =  url('admin/customers_shop/edit',$shop->customer_shop_id);


            $nestedData['user_id'] = $shop->first_name.' '.$shop->last_name;
            $nestedData['customer_business_person_name'] = $shop->customer_business_person_name;
            $nestedData['customer_business_person_number'] = $shop->customer_business_person_number;
            $nestedData['customer_rate_per_average_number_delivery_daily_base'] =$shop->customer_rate_per_average_number_delivery_daily_base;
            $nestedData['created_at'] = format_date_time($shop->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$shop->customer_shop_id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $user=User::where('role_id','=',3)->get();
        return view('customer_shop.add',compact('user'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'customer_business_type' => 'required',
        ],
        [
            'user_id.required' => 'User is Required',
            'customer_business_type.required' => 'Bussniss is Required',
         ]);
         if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $customer_shop=new CustomerShop();
            $customer_shop->user_id=$request->user_id;
            $customer_shop->customer_business_type=$request->customer_business_type;
            $customer_shop->customer_business_address=$request->customer_business_address;
            $customer_shop->customer_business_lat=$request->customer_business_lat;
            $customer_shop->customer_business_log=$request->customer_business_log;
            $customer_shop->customer_business_person_name=$request->customer_business_person_name;
            $customer_shop->customer_business_person_number=$request->customer_business_person_number;
            $customer_shop->customer_rate_per_delivery_in_boundary=$request->customer_rate_per_delivery_in_boundary;
            $customer_shop->customer_rate_per_delivery_out_boundary=$request->customer_rate_per_delivery_out_boundary;
            $customer_shop->customer_rate_per_average_number_delivery_daily_base=$request->customer_rate_per_average_number_delivery_daily_base;
            $customer_shop->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);
        }
    }
    public function edit($id)
    {

        $customer_shop=CustomerShop::where('customer_shop_id','=',$id)->first();
        if($customer_shop)
        {
            $user=User::where('role_id','=',3)->get();
            return view('customer_shop.edit',compact('customer_shop','user'));
        }
        return redirect()->back();

    }
    public function editProcess(Request $request)
    {
        $customer_shop=CustomerShop::where('customer_shop_id','=',$request->id)->first();
        if($customer_shop)
        {
            $validator = Validator::make($request->all(), [
                'user_id' => 'required',
                'customer_business_type' => 'required',
            ],
            [
                'user_id.required' => 'User is Required',
                'customer_business_type.required' => 'Bussniss is Required',
             ]);
             if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {

                $customer_shop->user_id=$request->user_id;
                $customer_shop->customer_business_type=$request->customer_business_type;
                $customer_shop->customer_business_address=$request->customer_business_address;
                $customer_shop->customer_business_lat=$request->customer_business_lat;
                $customer_shop->customer_business_log=$request->customer_business_log;
                $customer_shop->customer_business_person_name=$request->customer_business_person_name;
                $customer_shop->customer_business_person_number=$request->customer_business_person_number;
                $customer_shop->customer_rate_per_delivery_in_boundary=$request->customer_rate_per_delivery_in_boundary;
                $customer_shop->customer_rate_per_delivery_out_boundary=$request->customer_rate_per_delivery_out_boundary;
                $customer_shop->customer_rate_per_average_number_delivery_daily_base=$request->customer_rate_per_average_number_delivery_daily_base;
                $customer_shop->save();

                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
            }
        }
    }
    public function delete($id)
    {
      
        $delete=CustomerShop::where('customer_shop_id','=',$id)->first();
        dd($delete);
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }

}
