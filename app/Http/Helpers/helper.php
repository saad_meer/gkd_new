<?php
function db_format_date($date)
{
    if(!empty($date))
    {
        $formated_date=date('Y-m-d',strtotime($date));
        return $formated_date;
    }
}
function format_date($date)
{
    if(!empty($date))
    {
        $formated_date=date('d-m-Y',strtotime($date));
        return $formated_date;
    }
}

function format_date_time($date)
{
    if(!empty($date))
    {
        $formated_date=date('d-m-Y h:i a',strtotime($date));
        return $formated_date;
    }
}

