<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Address extends Model
{
    use SoftDeletes;
    public $table = 'user_to_address';

    public function addAddress($address,$id)
    {
        foreach($address as $key=>$value)
        {
            $address=new Address();
            $address->user_id=$id;
            $address->address_name=$value['address_name'];
            $address->address_number=$value['address_number'];
            $address->address_primary=$value['address_primary'];
            $address->address_area=$value['address_area'];
            $address->address_city=$value['address_city'];
            $address->address_state=$value['address_state'];
            $address->address_lat=$value['address_lat'];
            $address->address_log=$value['address_log'];
            $address->save();
        }
        return 'success';
    }
    public function updateAddress($address,$id)
    {
        Address::where('user_id',$id)->delete();
        foreach($address as $key=>$value)
        {
            $address=new Address();
            $address->user_id=$id;
            $address->address_name=$value['address_name'];
            $address->address_number=$value['address_number'];
            $address->address_primary=$value['address_primary'];
            $address->address_area=$value['address_area'];
            $address->address_city=$value['address_city'];
            $address->address_state=$value['address_state'];
            $address->address_lat=$value['address_lat'];
            $address->address_log=$value['address_log'];
            $address->save();
        }
        return 'success';
    }
}
