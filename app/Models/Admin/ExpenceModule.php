<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ExpenceModule extends Model
{
    use SoftDeletes;
    public $table = 'expence_module';
}
