
    <div class="modal fade" id="modal-addaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content tx-14">
            <div class="modal-header">
              <h6 class="modal-title" id="exampleModalLabel">Add Address</h6>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <input type="hidden" id="AddressRowID" value="0" />
                    <div class="form-group col-md-4">
                        <label>Name <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address_name">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Number <span class="text-danger">*</span></label>
                        <input type="number" class="form-control" id="address_number">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Address <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address_primary">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Area <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address_area">
                    </div>
                    <div class="form-group col-md-4">
                        <label>State <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address_state">
                    </div>
                    <div class="form-group col-md-4">
                        <label>City <span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="address_city">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Latitude</label>
                        <input type="text" class="form-control" id="address_lat">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Longitude </label>
                        <input type="text" class="form-control" id="address_log">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary tx-13" data-dismiss="modal">Close</button>
              <button type="button" onClick="addAddress()" class="btn btn-primary tx-13">Save changes</button>
            </div>
          </div>
        </div>
    </div>
