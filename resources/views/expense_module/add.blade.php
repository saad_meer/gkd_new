
@extends('layouts.header')
@section('title','Add Expence Module')
@section('content')


    <div class="content-body" >
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Expence Module</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/expense_module/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Expense Module" class="df-example demo-forms">
                        <form id="expense_module" action="{{url('admin/expense_module/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Person<span class="text-danger">*</span></label>
                                    <select class="form-control" name="expence_person">
                                        @foreach ($user as $value)
                                            <option value="{{$value->id}}">{{$value->first_name.' '.$value->last_name}}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="expence_name" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Amount<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="expence_amount" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Expence Date Time</label>
                                    <input type="text" class="form-control" name="expence_date_time" id="expence_date_time">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="expence_status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">Comments</label>
                                    <textarea class="form-control" name="expence_comment" id="" cols="20" rows="10"></textarea>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">
     $(function(){
        'use strict'
        $('#expence_date_time').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
    });

$('#expense_module').submit(function(event) {
   var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#expense_module');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#expense_module')[0].reset();
            swalsuccess(data.message,url+'/admin/expense_module/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



