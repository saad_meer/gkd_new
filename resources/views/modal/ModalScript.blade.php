<script>
function addAddress()
{
        var rowId = $('#AddressRowID').val();
        var address_name= $('#address_name').val();
        var address_number= $('#address_number').val();
        var address_primary= $('#address_primary').val();
        var address_area= $('#address_area').val();
        var address_state= $('#address_state').val();
        var address_city= $('#address_city').val();
        var address_lat= $('#address_lat').val();
        var address_log= $('#address_log').val();

        if(address_name == undefined || address_name == ''){
            toastr.error('Please enter Address Name');
            return false;
        }
        if(address_number == undefined || address_number == ''){
            toastr.error('Please enter Number');
            return false;
        }
        if(address_primary == undefined || address_primary == ''){
            toastr.error('Please enter Address');
            return false;
        }
        if(address_area == undefined || address_area == ''){
            toastr.error('Please enter Address Area');
            return false;
        }
        if(address_state == undefined || address_state == ''){
            toastr.error('Please enter Address State');
            return false;
        }
        if(address_city == undefined || address_city == ''){
            toastr.error('Please enter Address City');
            return false;
        }
        var rows = $("#addaddress").find('tr')
        var nextRow = parseInt(rows.length)+1;
        var delBtnLink = "<i class='fa fa-trash'></i>";

        if(rowId == 0){
            var html = '<tr id="address_row_'+nextRow+'">' +
                '<td><input class="address_name" type="hidden" name="addAddress['+nextRow+'][address_name]" value="'+address_name+'" />' +address_name+'</td>'+
                '<td><input class="address_number" type="hidden" name="addAddress['+nextRow+'][address_number]" value="'+address_number+'" />' +address_number+'</td>'+
                '<td><input class="address_primary" type="hidden" name="addAddress['+nextRow+'][address_primary]" value="'+address_primary+'" />' +address_primary+'</td>'+
                '<td><input class="address_area" type="hidden" name="addAddress['+nextRow+'][address_area]" value="'+address_area+'" />' +address_area+'</td>'+
                '<td><input class="address_state" type="hidden" name="addAddress['+nextRow+'][address_state]" value="'+address_state+'" />'+address_state+'</td>'+
                '<td><input class="address_city" type="hidden" name="addAddress['+nextRow+'][address_city]" value="'+address_city+'" />' +address_city+
                '<input class="address_lat" type="hidden" name="addAddress['+nextRow+'][address_lat]" value="'+address_lat+'" />'+
                '<input class="address_log" type="hidden" name="addAddress['+nextRow+'][address_log]" value="'+address_log+'" />'+
                  address_state+'</td>'+
                '<td width="100 px" >' +
                '<a class="ml-3"  type="button" onclick="removeAddressRow('+nextRow+')">'+delBtnLink+'</a>' +
                '</td>' +
                '</tr>';

            $("#addaddress").append(html);
        }
        else
        {
            nextRow = rowId;
            var html =   '<td><input class="address_name" type="hidden" name="addAddress['+nextRow+'][address_name]" value="'+address_name+'" />' +address_name+'</td>'+
                '<td><input class="address_number" type="hidden" name="addAddress['+nextRow+'][address_number]" value="'+address_number+'" />' +address_number+'</td>'+
                '<td><input class="address_primary" type="hidden" name="addAddress['+nextRow+'][address_primary]" value="'+address_primary+'" />' +address_primary+'</td>'+
                '<td><input class="address_area" type="hidden" name="addAddress['+nextRow+'][address_area]" value="'+address_area+'" />' +address_area+'</td>'+
                '<td><input class="address_state" type="hidden" name="addAddress['+nextRow+'][address_state]" value="'+address_state+'" />'+address_state+'</td>'+
                '<td><input class="address_city" type="hidden" name="addAddress['+nextRow+'][address_city]" value="'+address_city+'" />' +address_city+
                '<input class="address_lat" type="hidden" name="addAddress['+nextRow+'][address_lat]" value="'+address_lat+'" />'+
                '<input class="address_log" type="hidden" name="addAddress['+nextRow+'][address_log]" value="'+address_log+'" />'+
                  address_state+'</td>'+
                '<td width="100 px" >' +
                '<a class="ml-3"  type="button" onclick="removeAddressRow('+nextRow+')">'+delBtnLink+'</a>' +
                '</td>';

            $("#address_row_"+nextRow).html(html);
            toastr.success('Record Updated');
        }
        emptyRecordOfUrlModal();
        $("#address_name").val('');
        $("#address_number").val('');
        $("#address_primary").val('');
        $("#address_area").val('');
        $("#address_state").val('');
        $("#address_city").val('');
        $("#address_lat").val('');
        $("#address_log").val('');

        $('#modal-addaddress').modal('hide');
}
function removeAddressRow(rowId)
{
    $("#address_row_"+rowId).remove();
}
function emptyRecordOfUrlModal()
{
    $("#address_name").val('');
    $("#address_number").val('');
    $("#address_primary").val('');
    $("#address_area").val('');
    $("#address_state").val('');
    $("#address_city").val('');
    $("#address_lat").val('');
    $("#address_log").val('');
    $("#AddressRowID").val(0);
}
function editAddressModal(id)
    {
        var getRow = $("#address_row_"+id);
        var address_name=getRow.find('.address_name').val();
        var address_number=getRow.find('.address_number').val();
        var address_primary=getRow.find('.address_primary').val();
        var address_area=getRow.find('.address_area').val();
        var address_state=getRow.find('.address_state').val();
        var address_city=getRow.find('.address_city').val();
        var address_lat=getRow.find('.address_lat').val();
        var address_log=getRow.find('.address_log').val();


        $("#address_name").val(address_name);
        $("#address_number").val(address_number);
        $("#address_primary").val(address_primary);
        $("#address_area").val(address_area);
        $("#address_state").val(address_state);
        $("#address_city").val(address_city);
        $("#address_lat").val(address_lat);
        $("#address_log").val(address_log);
        $("#AddressRowID").val(id);

        $('#modal-addaddress').modal();

        return false;
    }
</script>
