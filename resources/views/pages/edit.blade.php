
@extends('layouts.header')
@section('title','Edit Page')

@section('content')
    <div class="content-body" style="overflow: hidden;">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Page</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/pages/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Page" class="df-example demo-forms">
                        <form id="editpage" action="{{url('admin/pages/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Title<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="pages_title" value="{{$page->pages_title}}" placeholder="First Name">
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="pages_status">
                                            <option value="Active" @if($page->pages_status=='Active') selected @endif>Active</option>
                                            <option value="Inactive" @if($page->pages_status=='Inactive') selected @endif>Inactive</option>
                                        </select>
                                </div>
                                <div class="col-md-12">
                                    <label>Discription</label>
                                    <textarea id="summernote" name="pages_description">{!!$page->pages_description!!}</textarea>

                                </div>
                                <div class="col-md-12 mt-5 pt-5">
                                    <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">


$('#editpage').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#editpage');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $page->id; ?>');

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false, 
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            // $('#editpage')[0].reset();
            swalsuccess(data.message,url+'/admin/pages/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



