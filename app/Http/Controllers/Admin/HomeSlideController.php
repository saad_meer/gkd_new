<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\HomeSlider;

class HomeSlideController extends Controller
{
    public function index()
    {
        return view('homeslider.list');
    }
    public function display(Request $request)
    {

        $columns = array(
            0 =>'position',
            1 =>'status',
            2=> 'id',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = HomeSlider::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $homeslider = HomeSlider::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $homeslider =  HomeSlider::where('position','LIKE',"%{$search}%")
                        ->orWhere('status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = HomeSlider::where('position','LIKE',"%{$search}%")
                        ->orWhere('status','LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($homeslider))
            {
            foreach ($homeslider as $homesliders)
            {
            $edit =  url('admin/homeslider/edit',$homesliders->id);
           // $delete =  url('/category/delete',$category->category_id);
            $image='';
            if($homesliders->image !='')
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src=" '.asset('storage/'.$homesliders->image).'" alt="" title=""></div>';
            }
            else
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src="'.asset('default_image.jpg').'" alt="" title=""></div>';
            }

            $nestedData['image'] = $image;
            $nestedData['position'] = $homesliders->position;
            $nestedData['status'] = $homesliders->status;
            $nestedData['created_at'] = format_date_time($homesliders->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$homesliders->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('homeslider.add');
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'position' => 'required|unique:category,category_name',
            'image' => 'mimes:jpg,jpeg,png',
        ],
        [
            'position.required' => 'Position is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $homeslider= new HomeSlider();
            $homeslider->position=$request->position;
            $homeslider->status=$request->status;
            if ($request->hasFile('image')) {
                $fileName = time().'_'.$request->image->getClientOriginalName();
                $filePath = $request->file('image')->storeAs('uploads', $fileName, 'public');
                $homeslider->image = $filePath;
            }
            $homeslider->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $homesliders=HomeSlider::where('id','=',$id)->first();
        if($homesliders)
        {
            return view('homeslider.edit',compact('homesliders'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {

        $homesliders=HomeSlider::where('id','=',$request->id)->first();
        if($homesliders)
        {
            $validator = Validator::make($request->all(), [
                'position' => 'required|unique:category,category_name',
                'image' => 'mimes:jpg,jpeg,png',
            ],
            [
                'position.required' => 'Position is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $homesliders->position=$request->position;
                $homesliders->status=$request->status;
                if ($request->hasFile('image')) {
                    $fileName = time().'_'.$request->image->getClientOriginalName();
                    $filePath = $request->file('image')->storeAs('uploads', $fileName, 'public');
                    $homesliders->image = $filePath;
                }
                $homesliders->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('admin/homeslider/display');

    }
    public function delete($id)
    {
        $homesliders=HomeSlider::where('id','=',$id)->first();
        if($homesliders)
        {
            $homesliders->delete();
            return response()->json(['code'=>404,'message'=>'Slider Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
