<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\DeliveryCharges;
use Validator;

class DeliveryChargesController extends Controller
{
    public function index()
    {
        return view('delivery_charges.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'delivery_charges_name',
            1 =>'delivery_charges_amount_charge',
            2=> 'delivery_charges_time_duration',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = DeliveryCharges::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $delivery_charges = DeliveryCharges::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $delivery_charges =  DeliveryCharges::where('delivery_charges_name','LIKE',"%{$search}%")
                        ->orWhere('delivery_charges_amount_charge', 'LIKE',"%{$search}%")
                        ->orWhere('delivery_charges_time_duration', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = DeliveryCharges::where('delivery_charges_name','LIKE',"%{$search}%")
                        ->orWhere('delivery_charges_amount_charge', 'LIKE',"%{$search}%")
                        ->orWhere('delivery_charges_time_duration', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($delivery_charges))
            {
            foreach ($delivery_charges as $delivery_charges)
            {
            $edit =  url('admin/delivery_charges/edit',$delivery_charges->id);

            $nestedData['name'] = $delivery_charges->delivery_charges_name;
            $nestedData['charges'] = $delivery_charges->delivery_charges_amount_charge;
            $nestedData['time'] = $delivery_charges->delivery_charges_time_duration;
            $nestedData['created_at'] = format_date_time($delivery_charges->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$delivery_charges->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('delivery_charges.add');
    }
    public function addProcess(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'delivery_charges_name' => 'required',
            'delivery_charges_amount_charge' => 'required',
            'delivery_charges_time_duration' => 'required',
        ],
        [
            'delivery_charges_name.required' => 'Name is Required',
            'delivery_charges_amount_charge.required' => 'Amount is Required',
            'delivery_charges_time_duration.required' => 'Time is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $delivery_charges= new DeliveryCharges();
            $delivery_charges->delivery_charges_name=$request->delivery_charges_name;
            $delivery_charges->delivery_charges_amount_charge=$request->delivery_charges_amount_charge;
            $delivery_charges->delivery_charges_time_duration=$request->delivery_charges_time_duration;
            $delivery_charges->delivery_charges_extra_charges=$request->delivery_charges_extra_charges;
            $delivery_charges->delivery_charges_note=$request->delivery_charges_note;
            $delivery_charges->save();

            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $delivery_charges=DeliveryCharges::where('id','=',$id)->first();
        if($delivery_charges)
        {
            return view('delivery_charges.edit',compact('delivery_charges'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {

        $delivery_charges=DeliveryCharges::where('id','=',$request->id)->first();
        if($delivery_charges)
        {
            $validator = Validator::make($request->all(), [
                'delivery_charges_name' => 'required',
                'delivery_charges_amount_charge' => 'required',
                'delivery_charges_time_duration' => 'required',
            ],
            [
                'delivery_charges_name.required' => 'Name is Required',
                'delivery_charges_amount_charge.required' => 'Amount is Required',
                'delivery_charges_time_duration.required' => 'Time is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {

                $delivery_charges->delivery_charges_name=$request->delivery_charges_name;
                $delivery_charges->delivery_charges_amount_charge=$request->delivery_charges_amount_charge;
                $delivery_charges->delivery_charges_time_duration=$request->delivery_charges_time_duration;
                $delivery_charges->delivery_charges_extra_charges=$request->delivery_charges_extra_charges;
                $delivery_charges->delivery_charges_note=$request->delivery_charges_note;
                $delivery_charges->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('delivery_charges/display');

    }
    public function delete($id)
    {
        $delivery_charges=DeliveryCharges::where('id','=',$id)->first();
        if($delivery_charges)
        {
            $delivery_charges->delete();
            return response()->json(['code'=>404,'message'=>'Delivery Charges Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }

}
