<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenceModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expence_module', function (Blueprint $table) {
            $table->id();
            $table->Integer('expence_person')->nullable();
            $table->text('expence_name')->nullable();
            $table->text('expence_amount')->nullable();
            $table->text('expence_date_time')->nullable();
            $table->text('expence_status')->nullable();
            $table->text('expence_comment')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expence_module');
    }
}
