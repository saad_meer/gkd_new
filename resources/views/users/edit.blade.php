
@extends('layouts.header')
@section('title','Edit Users')
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Users</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/users/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form id="addform" action="{{url('admin/users/editProcess')}}" method="POST">
                        <div data-label="Edit Users" class="df-example demo-forms">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">User Type<span class="text-danger">*</span></label>
                                    <select name="user_type" id="user_type" class="form-control">
                                        <option value="admin" @if($user->user_type=='admin') selected @endif>Admin</option>
                                        <option value="driver"  @if($user->user_type=='driver') selected @endif>Driver</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$user->first_name}}" name="first_name" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control"  value="{{$user->last_name}}" name="last_name" placeholder="Last Name">
                                </div>
                                {{-- <div class="form-group col-md-6">
                                    <label>Email<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" value="{{$user->email}}" name="email" placeholder="Email">
                                </div> --}}
                                <div class="form-group col-md-6">
                                    <label>Profile Picture</label>
                                    <input type="file" class="form-control" name="user_profile_picture" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Nic Front</label>
                                    <input type="file" class="form-control" name="user_nic_front" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Nic Back</label>
                                    <input type="file" class="form-control" name="user_nic_back" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Bank Check</label>
                                    <input type="text" class="form-control" value="{{$user->user_bank_check_number}}" name="user_bank_check_number" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Salary</label>
                                    <input type="text" class="form-control" value="{{$user->user_salary}}" name="user_salary" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Petrol Per Km</label>
                                    <input type="text"  class="form-control" value="{{$user->user_pertrol_per_km}}" name="user_pertrol_per_km" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>User Working Hour</label>
                                    <input type="number"  class="form-control" value="{{$user->user_working_hour}}" name="user_working_hour" >
                                </div>
                                <input type="hidden" name="old_user_profile_picture" value="{{$user->user_profile_picture}}">
                                <input type="hidden" name="old_user_nic_front" value="{{$user->user_nic_front}}">
                                <input type="hidden" name="old_user_nic_back" value="{{$user->user_nic_back}}">
                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="password_change" id="customCheck1" value="1">
                                        <label class="custom-control-label" for="customCheck1">Do You Want to Change Password</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6 d-none" id="password">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" placeholder="Password">
                                </div>
                            </div>
                        </div><!-- df-example -->
                        <div data-label="Add Address" class="df-example demo-forms mt-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4 class="mg-b-0 tx-spacing--1">Add Address</h4>
                                </div>
                                <div class="col-md-6 text-right">
                                    <a href="#modal-addaddress" type="button" data-toggle="modal"  class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Add</a>
                                </div>
                                <div class="col-sm-12 mt-4">
                                    <table class="table table-striped">
                                        <thead class="thead-primary">
                                            <tr>
                                                <th>Name</th>
                                                <th>Number</th>
                                                <th>Address</th>
                                                <th>Area</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tbody id="addaddress">
                                                @if(isset($address) && count($address)>0)
                                                    @foreach ($address as $key=>$value)
                                                        @php $key = $key+1; @endphp
                                                        <tr id="address_row_{{$key}}">
                                                            <td><input class="address_name" type="hidden"  name="addAddress[{{$key}}][address_name]" value="{{$value->address_name}}" /> <a href="#" onclick="return editAddressModal({{$key}})">{{$value->address_name}}</a></td>
                                                            <td><input class="address_number" type="hidden" name="addAddress[{{$key}}][address_number]" value="{{$value->address_number}}" />{{$value->address_number}}</td>
                                                            <td><input class="address_primary" type="hidden" name="addAddress[{{$key}}][address_primary]" value="{{$value->address_primary}}" />{{$value->address_primary}}</td>
                                                            <td><input class="address_area" type="hidden" name="addAddress[{{$key}}][address_area]" value="{{$value->address_area}}" />{{$value->address_area}}</td>
                                                            <td><input class="address_state" type="hidden" name="addAddress[{{$key}}][address_state]" value="{{$value->address_state}}" />{{$value->address_state}}</td>
                                                            <td><input class="address_city" type="hidden" name="addAddress[{{$key}}][address_city]" value="{{$value->address_city}}" />{{$value->address_city}}
                                                            <input class="address_lat" type="hidden" name="addAddress[{{$key}}][address_lat]" value="{{$value->address_lat}}" />
                                                            <input class="address_log" type="hidden" name="addAddress[{{$key}}][address_log]" value="{{$value->address_log}}" />
                                                              {{$value->address_state}}</td>
                                                            <td width="100 px" >
                                                            <a class="ml-3"  type="button" onclick="removeAddressRow({{$key}})"><i class="fa fa-trash"></i></a>
                                                            </td>

                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 float-right mt-4">
                                <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 mt-4">
                    <div data-label="Preview" class="df-example demo-forms">
                        <div class="form-row">
                            @if(!empty($user->user_profile_picture))
                            <div class="col-md-4">
                                <img src="{{url('storage/'.$user->user_profile_picture)}}" class="rounded float-left w-50" alt="">
                            </div>
                            @endif
                            @if(!empty($user->user_nic_front))
                            <div class="col-md-4">
                                <img src="{{url('storage/'.$user->user_nic_front)}}" class="rounded float-left w-50" alt="">
                            </div>
                            @endif
                            @if(!empty($user->user_nic_back))
                            <div class="col-md-4">
                                <img src="{{url('storage/'.$user->user_nic_back)}}" class="rounded float-left w-50" alt="">
                            </div>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- container -->
        @include('modal.ModalAddress');

    </div>

@endsection
@section('scripts')

<script type="text/javascript">
$("#customCheck1").change(function() {
    if(this.checked) {
        $('#password').removeClass('d-none');
    }
    else
    {
        $('#password').addClass('d-none');
    }
});

$('#addform').submit(function(event) {
    var url = '{{url("/")}}';

//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addform');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $user->id; ?>');

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addform')[0].reset();
            swalsuccess(data.message,url+'/admin/users/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});

</script>
@include('modal.ModalScript');

@endsection



