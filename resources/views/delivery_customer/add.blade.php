
@extends('layouts.header')
@section('title','Add Delivery Customer')
@section('styles')
<link href="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

<style>
    .bootstrap-tagsinput {
        display:block !important;
    }
</style>
@endsection
@section('content')
    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add  Delivery Customer</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/delivery/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Page" class="df-example demo-forms">
                        <form id="addpage" action="{{url('admin/delivery/addProcess')}}" method="POST">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Delivery Type<span class="text-danger">*</span></label>
                                    <select class="custom-select" name="delivery_type">
                                            <option value="Delivery">Delivery</option>
                                            <option value="Kharidari">Kharidari</option>
                                        </select>
                                </div>


                                <div class="form-group col-md-6">
                                    <label>Payment Type</label>
                                        <select class="custom-select" name="delivery_payment_type">
                                            <option value="cod">COD</option>
                                            <option value="notcod">NOt COD</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Amount</label>
                                    <input type="text" class="form-control" name="delivery_payment_amount" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Value</label>
                                    <input type="text" class="form-control" name="delivery_payment_value" >
                                </div>
                                <div class="col-md-12">
                                    <label>Payment Schedule</label>
                                    <input type="text" class="form-control" name="delivery_payment_schedules" id="schdeuledate" >

                                </div>
                                </div>
                     </div>
                     <div data-label="Pick up Address" class="df-example demo-forms">
                                <div class="col-md-12 px-0">
                                    <h4 class="mg-b-0 tx-spacing--1">Pick up Address</h4>
                                    <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Address Name</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_name" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>Address Number</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_number" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>Pick up Address</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_address" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>City</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_city" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>State</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_state" >
                                     </div>
                                     </div>
                                </div>
                            </div>
                            <div data-label="Drop Address" class="df-example demo-forms">
                                <div class="col-md-12 px-0">
                                <h4 class="mg-b-0 tx-spacing--1">Drop up Address</h4>
                                    <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Address Name</label>
                                        <input type="text" class="form-control" name="delivery_pick_up_address_state" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>Address Number</label>
                                        <input type="text" class="form-control" name="delivery_drop_off_address_number" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>Pick up Address</label>
                                        <input type="text" class="form-control" name="delivery_drop_off_address_address" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>City</label>
                                        <input type="text" class="form-control" name="delivery_drop_off_address_city" >
                                     </div>
                                     <div class="form-group col-md-6">
                                        <label>State</label>
                                        <input type="text" class="form-control" name="delivery_drop_off_address_state" >
                                     </div>
                                     </div>
                                </div>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                                </div>
                            </div>
                        </form>

                </div>
            </div>
        </div><!-- container -->
    </div>


@endsection
@section('scripts')
<script src="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">

$(function(){
        'use strict'
        $('#schdeuledate').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });


    });
$('#addpage').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#addpage');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#addpage')[0].reset();
            swalsuccess(data.message,url+'/admin/delivery/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



