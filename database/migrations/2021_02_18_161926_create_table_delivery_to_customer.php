<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDeliveryToCustomer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_to_customer', function (Blueprint $table) {
            $table->id();
            $table->text('delivery_type')->nullable();
            $table->text('delivery_payment_type')->nullable();
            $table->text('delivery_payment_amount')->nullable();
            $table->text('delivery_payment_value')->nullable();
            $table->text('delivery_payment_schedules')->nullable();
            $table->text('delivery_pick_up_address_name')->nullable();
            $table->text('delivery_pick_up_address_number')->nullable();
            $table->text('delivery_pick_up_address_address')->nullable();
            $table->text('delivery_pick_up_address_city')->nullable();
            $table->text('delivery_pick_up_address_state')->nullable();
            $table->text('delivery_drop_off_address_name')->nullable();
            $table->text('delivery_drop_off_address_number')->nullable();
            $table->text('delivery_drop_off_address_address')->nullable();
            $table->text('delivery_drop_off_address_city')->nullable();
            $table->text('delivery_drop_off_address_state')->nullable();
            $table->text('delivery_charges')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_to_customer');
    }
}
