
@extends('layouts.header')
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Edit Home Slider</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/homeslider/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Home Slider" class="df-example demo-forms">
                        <form id="addhomeslider" action="{{url('admin/homeslider/editProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Position<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="position" value="{{$homesliders->position}}" placeholder="Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="status">
                                            <option value="Active" @if($homesliders->position=='status') selected @endif>Active</option>
                                            <option value="Inactive" @if($homesliders->position=='Inactive') selected @endif>Inactive</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="image" id="customFile">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                    <div class="col-sm-12 mt-4">
                        @if(!empty($homesliders->image))
                        <div data-label="Preview" class="df-example demo-forms">
                            <div class="form-row">
                                <img src="{{url('storage/'.$homesliders->image)}}" class="rounded float-left w-25" alt="">
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#addhomeslider').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#addhomeslider');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $homesliders->id; ?>');
$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            swalsuccess(data.message,url+'/admin/homeslider/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



