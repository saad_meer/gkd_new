<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\Admin\Category;
use App\Models\Admin\SubCategory;


class CategoryController extends Controller
{
    public function index()
    {
        return view('category.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'category_name',
            1 =>'category_name',
            2=> 'category_status',
            3=> 'category_status',
            4=> 'created_at',
            5=> 'category_id',
        );

            $totalData = Category::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $category = Category::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $category =  Category::where('category_name','LIKE',"%{$search}%")
                        ->orWhere('category_meta', 'LIKE',"%{$search}%")
                        ->orWhere('category_status', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Category::where('category_name','LIKE',"%{$search}%")
                        ->orWhere('category_meta', 'LIKE',"%{$search}%")
                        ->orWhere('category_status', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($category))
            {
            foreach ($category as $category)
            {
            $edit =  url('admin/category/edit',$category->category_id);
           // $delete =  url('/category/delete',$category->category_id);
            $image='';
            if($category->category_image !='')
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src=" '.asset('storage/'.$category->category_image).'" alt="" title=""></div>';
            }
            else
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src="'.asset('default_image.jpg').'" alt="" title=""></div>';
            }

            $nestedData['image'] = $image;
            $nestedData['name'] = $category->category_name;
            $nestedData['meta'] = $category->category_meta;
            $nestedData['status'] =$category->category_status;
            $nestedData['created_at'] = format_date_time($category->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$category->category_id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        return view('category.add');
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_name' => 'required|unique:category,category_name',
            'category_image' => 'mimes:jpg,jpeg,png',
        ],
        [
            'category_name.required' => 'Category Name is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $category= new Category();
            $category->category_name=$request->category_name;
            $category->category_status=$request->category_status;
            if ($request->hasFile('category_image')) {
                $fileName = time().'_'.$request->category_image->getClientOriginalName();
                $filePath = $request->file('category_image')->storeAs('uploads', $fileName, 'public');
                $category->category_image = $filePath;
            }
            $category->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $category=Category::where('category_id','=',$id)->first();
        if($category)
        {
            return view('category.edit',compact('category'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {

        $category=Category::where('category_id','=',$request->category_id)->first();
        if($category)
        {
            $validator = Validator::make($request->all(), [
                'category_name' => 'required',
                'category_image' => 'mimes:jpg,jpeg,png',
            ],
            [
                'category_name.required' => 'Category Name is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $exists = Category::where('category_id','!=',$request->category_id)->where('category_name',$request->category_name)->count();
                if($exists) {
                    return response()->json(['code'=>404,'message'=>'Category Already exsists']);
                }
                $category->category_name=$request->category_name;
                $category->category_status=$request->category_status;
                if ($request->hasFile('category_image')) {
                    $fileName = time().'_'.$request->category_image->getClientOriginalName();
                    $filePath = $request->file('category_image')->storeAs('uploads', $fileName, 'public');
                    $category->category_image = $filePath;
                }
                $category->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('category/display');

    }
    public function delete($id)
    {
        $category=Category::where('category_id','=',$id)->first();
        if($category)
        {
            $category->delete();
            return response()->json(['code'=>404,'message'=>'Category Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
    

}
