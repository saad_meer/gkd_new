<?php

use Illuminate\Database\Seeder;
use App\Models\Admin\FontStyle;
use Illuminate\Support\Facades\Hash;

class FontsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'first_name' => 'Super Admin',
                'last_name' => 'Super Admin',
                'email' => 'Super Admin',
                'password' => Hash::make('admin123'),
            )
        ));
    }
}
