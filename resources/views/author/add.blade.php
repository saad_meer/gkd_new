
@extends('layouts.header')
@section('title','Add Author')
@section('styles')
<link href="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" rel="stylesheet">

<style>
    .bootstrap-tagsinput {
        display:block !important;
    }
</style>
@endsection
@section('content')
    <div class="content-body"  style="overflow: hidden;">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Author</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('author/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Author" class="df-example demo-forms">
                        <form id="author" action="{{url('author/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="author_first_name" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="author_last_name" placeholder="Last Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Date Of Birth</label>
                                    <input type="text" class="form-control" placeholder="Choose date" name="author_dob" id="author_dob">

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Date Of Death</label>
                                    <input type="text" class="form-control" placeholder="Choose date" name="author_death " id="author_death">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Date Of Death</label>
                                    <input type="text" class="form-control" placeholder="DC 700" name="author_death_text" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">References for Content (url)</label>
                                    <input type="text" class="form-control" name="author_reference_link" placeholder="References for Content (url)">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Nationality</label>
                                    <input type="text" class="form-control" name="author_nationality" placeholder="Nationality">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Meta</label>
                                    <input type="text" class="form-control" name="author_meta" placeholder="Meta">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>MetaData</label>
                                    <input type="text" class="form-control" name="author_meta_data" placeholder="MetaData">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Profession</label>
                                    <input type="text" class="form-control" value="" id="author_professions" name="author_professions" data-role="tagsinput">
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Status</label>
                                        <select class="custom-select" name="author_status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Image</label>
                                    <input type="file" class="form-control" name="author_image" id="customFile">
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Author Discription</label>
                                    <textarea id="summernote" name="author_description"></textarea>


                                </div>
                                <div class="col-md-12 mt-5 pt-5">
                                    <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                                </div>
                            </div>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')
<script src="{{url('lib/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>

<script type="text/javascript">
     $(function(){
        'use strict'
        $('#author_dob').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
        $('#author_death').datepicker({
            dateFormat: 'dd-mm-yy',
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true
        });
        $('#author_professions').tagsinput();


    });

$('#author').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#author');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#author')[0].reset();
            swalsuccess(data.message,url+'/author/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



