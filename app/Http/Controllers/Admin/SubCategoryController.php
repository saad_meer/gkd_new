<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Models\Admin\SubCategory;
use Validator;
class SubCategoryController extends Controller
{
    public function index()
    {
        return view('subcategory.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'subcategory_name',
            1 =>'subcategory_meta',
            2=> 'subcategory_meta',
            3=> 'subcategory_status',
            4=> 'created_at',
            5=> 'created_at',
            6=> 'id',
        );

            $totalData = SubCategory::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $sub_category = SubCategory::select('subcategory.*','category.category_id','category.category_name')
            ->leftjoin('category','category.category_id','=','subcategory.category_id')
            ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $sub_category =  SubCategory::select('subcategory.*','category.category_id','category.category_name')
                        ->leftjoin('category','category.category_id','=','subcategory.category_id')
                        ->where('subcategory_name','LIKE',"%{$search}%")
                        ->orWhere('category_meta', 'LIKE',"%{$search}%")
                        ->orWhere('category_status', 'LIKE',"%{$search}%")
                        ->orWhere('category_name', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered = SubCategory::select('subcategory.*','category.category_id','category.category_name')
                        ->leftjoin('category','category.category_id','=','subcategory.category_id')
                        ->where('subcategory_name','LIKE',"%{$search}%")
                        ->orWhere('category_meta', 'LIKE',"%{$search}%")
                        ->orWhere('category_status', 'LIKE',"%{$search}%")
                        ->orWhere('category_name', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($sub_category))
            {

            foreach ($sub_category as $sub_category)
            {
            $edit =  url('/sub-category/edit',$sub_category->id);
           // $delete =  url('/category/delete',$category->category_id);
            $image='';
            if($sub_category->subcategory_image !='')
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src=" '.asset('storage/'.$sub_category->subcategory_image).'" alt="" title=""></div>';
            }
            else
            {
                $image='<div class="avatar avatar-sm"><img class="rounded-circle" src="'.asset('default_image.jpg').'" alt="" title=""></div>';
            }

            $nestedData['image'] = $image;
            $nestedData['category'] =$sub_category->category_name;
            $nestedData['name'] = $sub_category->subcategory_name;
            $nestedData['meta'] = $sub_category->subcategory_meta;
            $nestedData['status'] =$sub_category->subcategory_status;
            $nestedData['created_at'] =  date('d-m-Y h:i a',strtotime($sub_category->created_at));
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$sub_category->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $category=Category::where('category_id','>',0)->get();
        return view('subcategory.add',compact('category'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'subcategory_name' => 'required|unique:subcategory,subcategory_name',
            'subcategory_image' => 'mimes:jpg,jpeg,png',
        ],
        [
            'category_id.required' => 'Please Select Category',
            'subcategory_name.required' => 'SubCategory Name is Required',
            'subcategory_name.unique' => 'SubCategory Already Exists',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $sub_category= new SubCategory();
            $sub_category->category_id=$request->category_id;
            $sub_category->subcategory_name=$request->subcategory_name;
            $sub_category->subcategory_meta=$request->subcategory_meta;
            $sub_category->subcategory_meta_data=$request->subcategory_meta_data;
            $sub_category->subcategory_status=$request->subcategory_status;
            if ($request->hasFile('subcategory_image')) {
                $fileName = time().'_'.$request->subcategory_image->getClientOriginalName();
                $filePath = $request->file('subcategory_image')->storeAs('uploads', $fileName, 'public');
                $sub_category->subcategory_image = $filePath;
            }
            $sub_category->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $sub_category=SubCategory::where('id','=',$id)->first();
        if($sub_category)
        {
            $category=Category::where('category_id','>',0)->get();
            return view('subcategory.edit',compact('sub_category','category'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $sub_category=SubCategory::where('id','=',$request->id)->first();
        if($sub_category)
        {
            $validator = Validator::make($request->all(), [
                'category_id' => 'required',
                'subcategory_name' => 'required|unique:subcategory,subcategory_name,'.$request->id,
                'subcategory_image' => 'mimes:jpg,jpeg,png',
            ],
            [
                'category_id.required' => 'Please Select Category',
                'subcategory_name.required' => 'SubCategory Name is Required',
                'subcategory_name.unique' => 'SubCategory Already Exists',

             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $sub_category->category_id=$request->category_id;
                $sub_category->subcategory_name=$request->subcategory_name;
                $sub_category->subcategory_meta=$request->subcategory_meta;
                $sub_category->subcategory_meta_data=$request->subcategory_meta_data;
                $sub_category->subcategory_status=$request->subcategory_status;
                if ($request->hasFile('subcategory_image')) {
                    $fileName = time().'_'.$request->subcategory_image->getClientOriginalName();
                    $filePath = $request->file('subcategory_image')->storeAs('uploads', $fileName, 'public');
                    $sub_category->subcategory_image = $filePath;
                }
                $sub_category->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('sub-category/display');

    }
    public function delete($id)
    {
        $sub_category=SubCategory::where('id','=',$id)->first();
        if($sub_category)
        {
            $sub_category->delete();
            return response()->json(['code'=>404,'message'=>'Sub Category Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
