<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerShop extends Model
{
    use SoftDeletes;
    public $table = 'customer_shop';
    protected $primaryKey = 'customer_shop_id ';
}
