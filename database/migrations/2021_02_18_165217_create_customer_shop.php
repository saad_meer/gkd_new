<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerShop extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_shop', function (Blueprint $table) {
            $table->id('customer_shop_id');
            $table->Integer('user_id')->nullable();
            $table->text('customer_business_type')->nullable();
            $table->text('customer_business_address')->nullable();
            $table->text('customer_business_lat')->nullable();
            $table->text('customer_business_log')->nullable();
            $table->text('customer_business_person_name')->nullable();
            $table->text('customer_business_person_number')->nullable();
            $table->text('customer_rate_per_delivery_in_boundary')->nullable();
            $table->text('customer_rate_per_delivery_out_boundary')->nullable();
            $table->text('customer_rate_per_average_number_delivery_daily_base')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_shop');
    }
}
