<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUsersPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('user_type')->nullable();
            $table->text('user_admin_notes')->nullable();
            $table->text('user_nic_front')->nullable();
            $table->text('user_nic_back')->nullable();
            $table->text('user_bank_check')->nullable();
            $table->text('user_profile_picture')->nullable();
            $table->text('user_salary')->nullable();
            $table->text('user_pertrol_per_km')->nullable();
            $table->text('user_working_hour')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('user_type');
            $table->dropColumn('user_admin_notes');
            $table->dropColumn('user_nic_front');
            $table->dropColumn('user_nic_back');
            $table->dropColumn('user_bank_check');
            $table->dropColumn('user_profile_picture');
            $table->dropColumn('user_salary');
            $table->dropColumn('user_pertrol_per_km');
            $table->dropColumn('user_working_hour');
            $table->dropColumn('deleted_at');
        });
    }
}
