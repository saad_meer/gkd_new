<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class DeliveryCustomer extends Model
{
    use SoftDeletes;
    public $table = 'delivery_to_customer';
}
