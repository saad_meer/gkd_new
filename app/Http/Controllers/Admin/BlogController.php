<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Category;
use App\Models\Admin\SubCategory;
use App\Models\Admin\Author;
use App\Models\Admin\Blog;
use Validator;
use File;
class BlogController extends Controller
{
    public function index()
    {
        return view('blog.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'blog_title',
            1 =>'blog_category_id',
            2 =>'blog_subcategory_id',
            3 =>'blog_status',
            4 =>'created_at',
            5=> 'id',
        );

            $totalData = Blog::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $blog = Blog::select('blogs.*','category.category_name','category.category_id','subcategory.subcategory_name','subcategory.id')
                    ->Leftjoin('category','category.category_id','=','blogs.blog_category_id')
                    ->Leftjoin('subcategory','subcategory.id','=','blogs.blog_subcategory_id')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $blog =  Blog::select('blogs.*','category.category_name','category.category_id','subcategory.subcategory_name','subcategory.id')
                        ->Leftjoin('category','category.category_id','=','blogs.blog_category_id')
                        ->Leftjoin('subcategory','subcategory.id','=','blogs.blog_subcategory_id')
                        ->where('blog_title','LIKE',"%{$search}%")
                        ->orWhere('blog_status', 'LIKE',"%{$search}%")
                        ->orWhere('category.category_name', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();
            $totalFiltered =  Blog::select('blogs.*','category.category_name','category.category_id','subcategory.subcategory_name','subcategory.id')
                            ->Leftjoin('category','category.category_id','=','blogs.blog_category_id')
                            ->Leftjoin('subcategory','subcategory.id','=','blogs.blog_subcategory_id')
                            ->where('blog_title','LIKE',"%{$search}%")
                            ->orWhere('blog_status', 'LIKE',"%{$search}%")
                            ->orWhere('category.category_name', 'LIKE',"%{$search}%")
                                ->count();
            }

            $data = array();
            if(!empty($blog))
            {

            foreach ($blog as $value)
            {

            $edit =  url('/blog/edit',$value->blog_id );

            $nestedData['title'] = $value->blog_title;
            $nestedData['category'] = $value->category_name;
            $nestedData['subcategory'] = $value->subcategory_name;
            $nestedData['status'] = $value->blog_status;
            $nestedData['created_at'] = format_date_time($value->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$value->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $category=Category::where('category_id','>',0)->get();
        $author=Author::where('id','>',0)->get();
        return view('blog.add',compact('category','author'));
    }
    public function addProcess(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'blog_title' => 'required',
            'blog_category_id' => 'required',
            'blog_author' => 'required',
        ],
        [
            'blog_title.required' => 'Titlle is Required',
            'blog_category_id.required' => 'Category is Required',
            'blog_author.required' => 'Author is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $blog=new Blog();
            $blog->blog_title=$request->blog_title;
            $blog->blog_category_id=$request->blog_category_id;
            $blog->blog_subcategory_id=$request->blog_subcategory_id;
            $blog->blog_author=$request->blog_author;
            $blog->blog_text=$request->blog_text;
            $blog->blog_status=$request->blog_status;

            if ($request->hasFile('blog_images')) {
                $fileName = time().'_'.$request->blog_images->getClientOriginalName();

                $filePath = $request->file('blog_images')->storeAs('uploads/blogimages', $fileName, 'public');
                $blog->blog_images = $filePath;
            }

            $blog->save();
            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);
        }
    }
    public function edit($id)
    {
        $blog=Blog::where('blog_id','=',$id)->first();
        if($blog)
        {
            $category=Category::where('category_id','>',0)->get();
            $sub_category=SubCategory::where('id','>',0)->get();
            $author=Author::where('id','>',0)->get();
            return view('blog.edit',compact('blog','category','author','sub_category'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {
        $blog=Blog::where('blog_id','=',$request->id)->first();
        if($blog)
        {
            $validator = Validator::make($request->all(), [
                'blog_title' => 'required',
                'blog_category_id' => 'required',
                'blog_author' => 'required',
            ],
            [
                'blog_title.required' => 'Titlle is Required',
                'blog_category_id.required' => 'Category is Required',
                'blog_author.required' => 'Author is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {
                $blog->blog_title=$request->blog_title;
                $blog->blog_category_id=$request->blog_category_id;
                $blog->blog_subcategory_id=$request->blog_subcategory_id;
                $blog->blog_author=$request->blog_author;
                $blog->blog_text=$request->blog_text;
                $blog->blog_status=$request->blog_status;

                if ($request->hasFile('blog_images')) {
                    File::delete(public_path('storage/'.$request->old_image));
                    $fileName = time().'_'.$request->blog_images->getClientOriginalName();

                    $filePath = $request->file('blog_images')->storeAs('uploads/blogimages', $fileName, 'public');
                    $blog->blog_images = $filePath;
                }

                $blog->save();
                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);
            }


        }
        return redirect('blog/display');

    }
    public function delete($id)
    {
        $delete=Blog::where('blog_id','=',$id)->first();
        if($delete)
        {
            $delete->delete();
            return response()->json(['code'=>404,'message'=>'Record Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
