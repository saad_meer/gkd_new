<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class FontStyle extends Model
{
    public $table = 'fonts_style';
}
