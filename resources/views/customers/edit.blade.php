
@extends('layouts.header')
@section('title','Edit Customer')
@section('content')
<div class="content-body" >
    <div class="container pd-x-0">
        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
        <div>
            <h4 class="mg-b-0 tx-spacing--1">Edit Customer</h4>
        </div>
        <div class="d-none d-md-block">
            <a href="{{url('admin/customers/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
        </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form id="customers" action="{{url('admin/customers/editProcess')}}" method="POST" >

                    <div data-label="Customer" class="df-example demo-forms">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">First Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="first_name" value="{{$user->first_name}}" placeholder="First Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Last Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="last_name" value="{{$user->last_name}}" placeholder="Last Name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Phone Number 1</label>
                                    <input type="number" class="form-control"  name="phone_number_one" value="{{$user->phone_number_one}}" id="phone_number_one">

                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Phone Number 2</label>
                                    <input type="number" class="form-control"  name="phone_number_two" value="{{$user->phone_number_two}}" id="phone_number_two">
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="password_change" id="customCheck1" value="1">
                                        <label class="custom-control-label" for="customCheck1">Do You Want to Change Password</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Password</label>
                                    <input type="password" class="form-control" name="password" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Profile Photo</label>
                                    <input type="file" class="form-control" name="user_profile_picture" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Notes</label>
                                    <input type="text" class="form-control" name="user_admin_notes" placeholder="Notes" value="{{$user->user_admin_notes}}">
                                </div>

                            </div>
                    </div><!-- df-example -->
                    <div data-label="Edit Address" class="df-example demo-forms mt-4">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mg-b-0 tx-spacing--1">Add Address</h4>
                            </div>
                            <div class="col-md-6 text-right">
                                <a href="#modal-addaddress" type="button" data-toggle="modal"  class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Add</a>
                            </div>
                            <div class="col-sm-12 mt-4">
                                <table class="table table-striped">
                                    <thead class="thead-primary">
                                        <tr>
                                            <th>Name</th>
                                            <th>Number</th>
                                            <th>Address</th>
                                            <th>Area</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tbody id="addaddress">
                                            @if(isset($address) && count($address)>0)
                                                @foreach ($address as $key=>$value)
                                                    @php $key = $key+1; @endphp
                                                    <tr id="address_row_{{$key}}">
                                                        <td><input class="address_name" type="hidden"  name="addAddress[{{$key}}][address_name]" value="{{$value->address_name}}" /> <a href="#" onclick="return editAddressModal({{$key}})">{{$value->address_name}}</a></td>
                                                        <td><input class="address_number" type="hidden" name="addAddress[{{$key}}][address_number]" value="{{$value->address_number}}" />{{$value->address_number}}</td>
                                                        <td><input class="address_primary" type="hidden" name="addAddress[{{$key}}][address_primary]" value="{{$value->address_primary}}" />{{$value->address_primary}}</td>
                                                        <td><input class="address_area" type="hidden" name="addAddress[{{$key}}][address_area]" value="{{$value->address_area}}" />{{$value->address_area}}</td>
                                                        <td><input class="address_state" type="hidden" name="addAddress[{{$key}}][address_state]" value="{{$value->address_state}}" />{{$value->address_state}}</td>
                                                        <td><input class="address_city" type="hidden" name="addAddress[{{$key}}][address_city]" value="{{$value->address_city}}" />{{$value->address_city}}
                                                        <input class="address_lat" type="hidden" name="addAddress[{{$key}}][address_lat]" value="{{$value->address_lat}}" />
                                                        <input class="address_log" type="hidden" name="addAddress[{{$key}}][address_log]" value="{{$value->address_log}}" />
                                                        {{$value->address_state}}</td>
                                                        <td width="100 px" >
                                                        <a class="ml-3"  type="button" onclick="removeAddressRow({{$key}})"><i class="fa fa-trash"></i></a>
                                                        </td>

                                                    </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-5 pt-5">
                        <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                    </div>
                    <div class="col-sm-12 mt-4">
                        @if(!empty($user->user_profile_picture))
                        <div data-label="Preview" class="df-example demo-forms">
                            <div class="form-row">
                                <img src="{{url('storage/'.$user->user_profile_picture)}}" class="rounded float-left w-25" alt="">
                            </div>
                        </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div><!-- container -->
</div>
@include('modal.ModalAddress')
@endsection
@section('scripts')
<script type="text/javascript">

$('#customers').submit(function(event) {
    var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();
var frm = $('#customers');

var formData = new FormData($(this)[0]);
formData.append('id','<?php echo $user->id; ?>');

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
           // $('#author')[0].reset();
           swalsuccess(data.message,url+'/admin/customers/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@include('modal.ModalScript')
@endsection



