<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Admin\ExpenceModule;
use Validator;

class ExpenceModuleController extends Controller
{
    public function index()
    {
        return view('expense_module.list');
    }
    public function display(Request $request)
    {
        $columns = array(
            0 =>'expence_name',
            1 =>'expence_amount',
            2=> 'id',
            3=> 'created_at',
            4=> 'id',
        );

            $totalData = ExpenceModule::count();

            $totalFiltered = $totalData;

            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
            $expence_module = ExpenceModule::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
            }
            else {
            $search = $request->input('search.value');

            $expence_module =  ExpenceModule::Leftjoin('users','users.id','expence_module.expence_person')
                        ->where('expence_name','LIKE',"%{$search}%")
                        ->orWhere('expence_amount', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $expence_module =  ExpenceModule::Leftjoin('users','users.id','expence_module.expence_person')
                        ->where('expence_name','LIKE',"%{$search}%")
                        ->orWhere('expence_amount', 'LIKE',"%{$search}%")
                        ->count();
            }

            $data = array();
            if(!empty($expence_module))
            {
            foreach ($expence_module as $values)
            {
            $edit =  url('admin/expense_module/edit',$values->id);

            $nestedData['user'] = $values->first_name.' '.$values->last_name;
            $nestedData['name'] = $values->expence_name;
            $nestedData['amount'] = $values->expence_amount;
            $nestedData['created_at'] = format_date_time($values->created_at);
            $nestedData['options'] = '<div class="dropdown">
            <a class=" dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Action
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="'.$edit.'"><i class="fas fa-edit"></i>  Edit</a>
              <a class="dropdown-item"onClick="deleterow('.$values->id.')"><i class="fas fa-trash"></i> Delete</a>
            </div>
          </div>';
            $data[] = $nestedData;

            }
            }

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );

            echo json_encode($json_data);
    }
    public function add()
    {
        $user=User::where('role_id',3)->get();
        return view('expense_module.add',compact('user'));
    }
    public function addProcess(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'expence_person' => 'required',
            'expence_name' => 'required',
            'expence_amount' => 'required',
        ],
        [
            'expence_person.required' => 'User is Required',
            'expence_name.required' => 'Name is Required',
            'expence_amount.required' => 'Amount is Required',
         ]);
        if ($validator->fails()) {
            return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
        }
        else
        {
            $expence_module= new ExpenceModule();
            $expence_module->expence_person=$request->expence_person;
            $expence_module->expence_name=$request->expence_name;
            $expence_module->expence_amount=$request->expence_amount;
            $expence_module->expence_date_time=$request->expence_date_time;
            $expence_module->expence_status=$request->expence_status;
            $expence_module->expence_comment=$request->expence_comment;
            $expence_module->save();

            return response()->json(['code'=>200,'message'=>'Record Added Successfully']);


        }
    }
    public function edit($id)
    {
        $expense_module=ExpenceModule::where('id','=',$id)->first();
        if($expense_module)
        {
            $user=User::where('role_id',3)->get();
            return view('expense_module.edit',compact('expense_module','user'));
        }
        return redirect()->back();
    }
    public function editProcess(Request $request)
    {

        $expence_module=ExpenceModule::where('id','=',$request->id)->first();
        if($expence_module)
        {
            $validator = Validator::make($request->all(), [
                'expence_person' => 'required',
                'expence_name' => 'required',
                'expence_amount' => 'required',
            ],
            [
                'expence_person.required' => 'User is Required',
                'expence_name.required' => 'Name is Required',
                'expence_amount.required' => 'Amount is Required',
             ]);
            if ($validator->fails()) {
                return response()->json(['code'=>404,'message'=>$validator->errors()->first()]);
            }
            else
            {

                $expence_module->expence_person=$request->expence_person;
                $expence_module->expence_name=$request->expence_name;
                $expence_module->expence_amount=$request->expence_amount;
                $expence_module->expence_date_time=$request->expence_date_time;
                $expence_module->expence_status=$request->expence_status;
                $expence_module->expence_comment=$request->expence_comment;
                $expence_module->save();


                return response()->json(['code'=>200,'message'=>'Record Updated Successfully']);


            }
        }
        return redirect('expense_module/display');

    }
    public function delete($id)
    {
        $values=ExpenceModule::where('id','=',$id)->first();
        if($values)
        {
            $values->delete();
            return response()->json(['code'=>404,'message'=>'Delivery Charges Deleted Successfully']);
        }
        else
        {
            return response()->json(['code'=>404,'message'=>'Record Not Found']);
        }
    }
}
