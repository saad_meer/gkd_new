
@extends('layouts.header')
@section('title','Add Delivery Charges')
@section('content')


    <div class="content-body" >
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Add Delivery Charges</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/delivery_charges/display')}}" class="btn btn-sm pd-x-15 btn-light btn-uppercase mg-l-5 active"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i>Back</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                    <div data-label="Delivery Charges" class="df-example demo-forms">
                        <form id="delivery_charges" action="{{url('admin/delivery_charges/addProcess')}}" method="POST">

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Name<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="delivery_charges_name" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Charges<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="delivery_charges_amount_charge" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Time Duration<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" name="delivery_charges_time_duration" >
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Extra Charges</label>
                                    <input type="text" class="form-control" name="delivery_charges_extra_charges" >
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">Note</label>
                                    <textarea class="form-control" name="delivery_charges_note" id="" cols="20" rows="10"></textarea>
                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary active"><i data-feather="save" class="wd-10 mg-r-5"></i>Submit</button>
                        </form>
                    </div><!-- df-example -->
                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection
@section('scripts')

<script type="text/javascript">


$('#delivery_charges').submit(function(event) {
   var url = '{{url("/")}}';
//prevent the form from submitting by default
event.preventDefault();

var frm = $('#delivery_charges');

var formData = new FormData($(this)[0]);

$.ajax({
    url: frm.attr('action'),
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
        if(data.code==200)
        {
            $('#delivery_charges')[0].reset();
            swalsuccess(data.message,url+'/admin/delivery_charges/display');
        }
        if(data.code==404)
        {
            swalerror(data.message);
        }
    },
    error: function (error) {
        swalerror("SomeThing Went Wrong");
    }
});



});
</script>
@endsection



