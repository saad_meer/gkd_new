
@extends('layouts.header')
@section('title','Expence Module')
@section('datatables')
<link href="{{url('lib/datatables.net-dt/css/jquery.dataTables.min.css')}}" rel="stylesheet">
<link href="{{url('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css')}}" rel="stylesheet">

@endsection
@section('content')


    <div class="content-body">
        <div class="container pd-x-0">
            <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
            <div>
                <h4 class="mg-b-0 tx-spacing--1">Expence Module</h4>
            </div>
            <div class="d-none d-md-block">
                <a href="{{url('admin/expense_module/add')}}" class="btn btn-sm pd-x-15 btn-primary btn-uppercase mg-l-5 active"><i data-feather="plus" class="wd-10 mg-r-5"></i>Add</a>
            </div>
            </div>
            <div class="row">
                <div class="col-sm-12">

                        <table id="data_display" class="table text-center">
                          <thead>
                            <tr>
                              <th>User</th>
                              <th>Name</th>
                              <th>Amount</th>
                              <th>Created_at</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                            <tbody>

                            </tbody>
                        </table>

                </div>
            </div>
        </div><!-- container -->
    </div>

@endsection

@section('scripts')
<script src="{{url('lib/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-dt/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{url('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js')}}"></script>


<script>
    $(document).ready(function () {
        $('#data_display').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                     "url": "{{ url('admin/expense_module/display') }}",
                     "dataType": "json",
                     "type": "POST",
                   },
            "columns": [
                { "data": "user" },
                { "data": "name" },
                { "data": "amount" },
                { "data": "created_at" },
                { "data": "options" }
            ]

        });
    });
    function deleterow(id)
    {
        swal({
        title: 'Are you sure You want to delete?',
        icon: "warning",
        ClassName:"swal-title",
        showCancelButton: false,
        showConfirmButton: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                    var url = '{{url("/")}}';
                    $.ajax({
                    type: 'GET',
                    url:  url+'/admin/expense_module/delete/'+id,
                    success: function(data) {
                        swalsuccesscurrent(data.message);
                    },
                    error: function(xhr) { // if error occured
                        swalerror("SomeThing Went Wrong");
                    },
                    });
                        } else {
                        return false;

                        }
                    });

    }
</script>

@endsection

